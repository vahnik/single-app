extern crate time;

use std::error::Error;
use std::thread;
use std::thread::JoinHandle;
use time::PreciseTime;
use std::fs::File;
use std::io::Read;
use std::str::Lines;
use std::iter::Map;
use std::option::Option::Some;

#[derive(Debug)]
struct Bar {
    ticker: String,
    year: u16,
    month: u8,
    day: u8,
}

fn fut_year(s: &str) -> JoinHandle<u16> {
    let s = s.to_string();
    thread::spawn(move || s[0..4].parse::<u16>().unwrap())
}
fn fut_month(s: &str) -> JoinHandle<u8> {
    let s = s.to_string();
    thread::spawn(move || s[4..6].parse::<u8>().unwrap())
}
fn fut_day(s: &str) -> JoinHandle<u8> {
    let s = s.to_string();
    thread::spawn(move || s[6..8].parse::<u8>().unwrap())
}
fn bar(csv_row: &str) -> Result<Bar, Box<Error>>  {
    let split = csv_row.split(";").enumerate();


    let mut ticker: Option<&str> = None;
    let mut y: Option<JoinHandle<u16>> = None;
    let mut m: Option<JoinHandle<u8>> = None;
    let mut d: Option<JoinHandle<u8>> = None;
    for s in split {
        //        println!("s:{:?}", s);
        match s {
            (0, v) => ticker = Some(v),
            (2, v) => {
                y = Some(fut_year(v));
                m = Some(fut_month(v));
                d = Some(fut_day(v));
            }
            (_, _) => {
                //println!("{}:{}", n, v)
            }
        }
    }
    //    println!("ticker is null:{}", ticker.is_some());
//        Ok(Bar {
//            ticker: "ticker".to_string(),
//            year: 11,
//            month: 1,
//            day: 1,
//        })
        Ok(Bar {
            ticker: ticker.expect("cannot get ticker").to_string(),
            year: 11,
            month: 1,
            day: 1,
        })
//    Ok(Bar {
//        ticker: ticker.expect("cannot get ticker").to_string(),
//        year: y.expect("cannot get year").join().unwrap(),
//        month: m.expect("cannot get month").join().unwrap(),
//        day: d.expect("cannot get day").join().unwrap(),
//    })

}

//fn parse(csv: String) -> Map<Lines, Bar> {
fn parse(csv: String) {
    let b = csv.lines().map(|line| {
//        println!("parse");
        let b = match bar(line) {
            Ok(res) => Some(res),
            Err(e) => {
                println!("There was an error:{:?}", e);
                None
            }
        };
        b
    }).last();
    println!("last:{:?}", b);
}
fn read_file(path: &str) -> String{
    let mut f = File::open(path).expect("cannot find file");
    let mut content = String::new();
    f.read_to_string(&mut content).expect("cannot read file");
    content
}
fn main() {
    println!("main");
    let s = PreciseTime::now();
//    let bar = parse("10Y T-Note INT Rates;1;201a71211;152000;2.099;2.099;2.099;2.099;0");
//    println!(
//        "result:{:?}",
//        bar
//    );
    let file = "data/a.txt";
    parse(read_file(file));
    println!("File reading:{}, ms", s.to(PreciseTime::now()).num_milliseconds());
}
